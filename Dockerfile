FROM maven:3.8-openjdk-11 AS build
WORKDIR /app
COPY . .
RUN mvn clean package
 
FROM tomcat:9.0-jre11 AS deploy
COPY --from=build /app/target/*.war /usr/local/tomcat/webapps/
 
EXPOSE 8080
 
CMD ["catalina.sh", "run"]
